﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class GluttonyQuiz : Form
    {
        public GluttonyQuiz()
        {
            InitializeComponent();
        }

        int n = 0;
        bool[] answ = new bool[5];
        private void NextButton_Click(object sender, EventArgs e)
        {
            if (n == 0)
            {
                QBar.Increment(15);
                NextButton.Text = "next";

                Answer1.Visible = true;
                Answer2.Visible = true;
                Answer3.Visible = true;
                Answer4.Visible = true;
                Question.Visible = true;
                Counter.Visible = true;

                Counter.Text = "1/5 вопрос";
                Question.Text = "Год начала первого\nкрестового похода";
                Answer1.Text = "1096";
                Answer2.Text = "1247";
                Answer3.Text = "1812";
                Answer4.Text = "996";
                n++;
            }
            else if (n == 1)
            {
                QBar.Increment(15);
                answ[0] = Answer1.Checked == true ? true : false;
                Answer1.Checked = false;
                Answer2.Checked = false;
                Answer3.Checked = false;
                Answer4.Checked = false;

                Counter.Text = "2/5 вопрос";
                Question.Text = "";
                Question.ImageList = imageList1;
                Question.ImageIndex = 0;
                Answer1.Text = "Иероним Босх\n\"Ад\"";
                Answer2.Text = "Иероним Босх\n\"Сад земных\nнаслаждений\"";
                Answer3.Text = "Сандро Боттичелли\n\"Круги ада\"";
                Answer4.Text = "Бернард Пикарт\n\"Сцена ада\"";

                n++;
            }
            else if (n == 2)
            {
                QBar.Increment(15);
                answ[1] = Answer3.Checked == true ? true : false;
                Answer1.Checked = false;
                Answer2.Checked = false;
                Answer3.Checked = false;
                Answer4.Checked = false;

                Counter.Text = "3/5 вопрос";
                Question.Text = "Фома Аквинский";
                Question.ImageList = null;
                Answer1.Text = "";
                Answer2.Text = "";
                Answer3.Text = "";
                Answer4.Text = "";
                pictureBox1.Visible = true;
                pictureBox2.Visible = true;
                pictureBox3.Visible = true;
                pictureBox4.Visible = true;

                n++;
            }
            else if (n == 3)
            {
                QBar.Increment(15);
                answ[2] = Answer4.Checked == true ? true : false;
                Answer1.Checked = false;
                Answer2.Checked = false;
                Answer3.Checked = false;
                Answer4.Checked = false;

                Counter.Text = "4/5 вопрос";
                Question.Text = "Вставьте пропуск\nМои предки ... имперцы. А ваши?";
                Question.ImageList = null;
                Answer1.Text = "гордятся мной";
                Answer2.Text = "улыбаются глядя\nна меня";
                Answer3.Text = "со мной";
                Answer4.Text = "наблюдают за\nмной";
                pictureBox1.Visible = false;
                pictureBox2.Visible = false;
                pictureBox3.Visible = false;
                pictureBox4.Visible = false;

                n++;
            }
            else if (n == 4)
            {
                QBar.Increment(15);
                answ[3] = Answer2.Checked == true ? true : false;
                Answer1.Checked = false;
                Answer2.Checked = false;
                Answer3.Checked = false;
                Answer4.Checked = false;

                pictureBox1.Visible = false;
                pictureBox2.Visible = false;
                pictureBox3.Visible = false;
                pictureBox4.Visible = false;

                Counter.Text = "5/5 вопрос";
                Question.Text = "Копия без оригинала?";
                Answer1.Text = "Симулякр";
                Answer2.Text = "Симуляция";
                Answer3.Text = "Фантазм";
                Answer4.Text = "Дазайн";

                n++;
            } else
            {
                QBar.Increment(30);
                answ[4] = Answer1.Checked == true ? true : false;

                Counter.Text = "Результат";

                Answer1.Visible = false;
                Answer2.Visible = false;
                Answer3.Visible = false;
                Answer4.Visible = false;
                NextButton.Visible = false;

                int right = 0;
                for (int i = 0; i < 5; i++)
                {
                    if (answ[i] == true) right++;
                }
                Question.AutoSize = true;
                Question.Text = right.ToString() + " из 5 верных ответов\n";
                Question.Text += answ[0] == true ? "1 - верный ответ\n" : "1 - неверный ответ\n";
                Question.Text += answ[1] == true ? "2 - верный ответ\n" : "2 - неверный ответ\n";
                Question.Text += answ[2] == true ? "3 - верный ответ\n" : "3 - неверный ответ\n";
                Question.Text += answ[3] == true ? "4 - верный ответ\n" : "4 - неверный ответ\n";
                Question.Text += answ[4] == true ? "5 - верный ответ\n" : "5 - неверный ответ\n";

                Question.Location = new Point(12, 52);

                if (right < 2)
                {
                    FinalImage.ImageIndex = 5;
                    Question.Text += "Ты простой крестьянин\nзнать что-либо тебе\nне положено";
                }
                else if (right < 4)
                {
                    FinalImage.ImageIndex = 6;
                    Question.Text += "Ты феодал\nне сильно умнее\nкрестьнина, но\nчто-то слышал\nот священника";
                }
                else
                {
                    FinalImage.ImageIndex = 7;
                    Question.Text += "Ты священник\nмного знаешь,\nна столько же во многом\nзаблуждаешься";
                }

            }
        }
    }
}
