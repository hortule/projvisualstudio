﻿namespace LabMyNotepad
{
    partial class LabMyNotepad
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.miNotes = new System.Windows.Forms.ToolStripMenuItem();
            this.miCreateNote = new System.Windows.Forms.ToolStripMenuItem();
            this.miOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.дневникТёрнераToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.староеВремяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.новоеВремяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miNotes,
            this.miOptions,
            this.справкаToolStripMenuItem,
            this.дневникТёрнераToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(492, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // miNotes
            // 
            this.miNotes.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCreateNote});
            this.miNotes.Name = "miNotes";
            this.miNotes.Size = new System.Drawing.Size(65, 20);
            this.miNotes.Text = "Заметки";
            this.miNotes.Click += new System.EventHandler(this.miNotes_Click);
            this.miNotes.MouseEnter += new System.EventHandler(this.заметкиToolStripMenuItem_MouseEnter);
            // 
            // miCreateNote
            // 
            this.miCreateNote.Name = "miCreateNote";
            this.miCreateNote.Size = new System.Drawing.Size(180, 22);
            this.miCreateNote.Text = "Создать";
            // 
            // miOptions
            // 
            this.miOptions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.настройкиToolStripMenuItem});
            this.miOptions.Name = "miOptions";
            this.miOptions.Size = new System.Drawing.Size(95, 20);
            this.miOptions.Text = "Инструменты";
            this.miOptions.Click += new System.EventHandler(this.miOptions_Click_1);
            // 
            // настройкиToolStripMenuItem
            // 
            this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
            this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.настройкиToolStripMenuItem.Text = "Настройки";
            this.настройкиToolStripMenuItem.Click += new System.EventHandler(this.настройкиToolStripMenuItem_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAbout});
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            // 
            // miAbout
            // 
            this.miAbout.Name = "miAbout";
            this.miAbout.Size = new System.Drawing.Size(180, 22);
            this.miAbout.Text = "Об авторе";
            this.miAbout.Click += new System.EventHandler(this.miAbout_Click);
            // 
            // дневникТёрнераToolStripMenuItem
            // 
            this.дневникТёрнераToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.староеВремяToolStripMenuItem});
            this.дневникТёрнераToolStripMenuItem.Name = "дневникТёрнераToolStripMenuItem";
            this.дневникТёрнераToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.дневникТёрнераToolStripMenuItem.Text = "1984";
            // 
            // староеВремяToolStripMenuItem
            // 
            this.староеВремяToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.новоеВремяToolStripMenuItem});
            this.староеВремяToolStripMenuItem.Name = "староеВремяToolStripMenuItem";
            this.староеВремяToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.староеВремяToolStripMenuItem.Text = "1917";
            this.староеВремяToolStripMenuItem.Click += new System.EventHandler(this.староеВремяToolStripMenuItem_Click);
            // 
            // новоеВремяToolStripMenuItem
            // 
            this.новоеВремяToolStripMenuItem.Name = "новоеВремяToolStripMenuItem";
            this.новоеВремяToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.новоеВремяToolStripMenuItem.Text = "1937";
            // 
            // LabMyNotepad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 542);
            this.Controls.Add(this.menuStrip1);
            this.Cursor = System.Windows.Forms.Cursors.UpArrow;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "LabMyNotepad";
            this.Text = "Мои заметки";
            this.Load += new System.EventHandler(this.LabMyNotepad_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miNotes;
        private System.Windows.Forms.ToolStripMenuItem miCreateNote;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miAbout;
        private System.Windows.Forms.ToolStripMenuItem дневникТёрнераToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem староеВремяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem новоеВремяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miOptions;
        private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem;
    }
}

