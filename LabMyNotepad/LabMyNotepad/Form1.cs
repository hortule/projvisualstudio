﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabMyNotepad
{
    public partial class LabMyNotepad : Form
    {
        public LabMyNotepad()
        {
            InitializeComponent();
            //
            miCreateNote.Click += MiCreateNote_Click;
            miAbout.Click += MiAbout_Click;
            miOptions.Click += MiOptions_Click;
        }

        private void MiOptions_Click(object sender, EventArgs e)
        {
        }

        private void MiAbout_Click(object sender, EventArgs e)
        {

        }

        private void MiCreateNote_Click(object sender, EventArgs e)
        {
            fmNote x = new fmNote();
            x.MdiParent = this;
            x.Show();
            
        }

        private void LabMyNotepad_Load(object sender, EventArgs e)
        {
            
        }

        private void заметкиToolStripMenuItem_MouseEnter(object sender, EventArgs e)
        {
            
        }

        private void miAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Denis Mishchenko");
        }

        private void настройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void miNotes_Click(object sender, EventArgs e)
        {
 
        }

        private void miOptions_Click_1(object sender, EventArgs e)
        {

        }

        private void староеВремяToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
