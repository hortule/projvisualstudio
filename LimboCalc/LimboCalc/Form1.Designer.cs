﻿namespace LimboCalc
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SevenB = new System.Windows.Forms.Button();
            this.NineB = new System.Windows.Forms.Button();
            this.EightB = new System.Windows.Forms.Button();
            this.FourB = new System.Windows.Forms.Button();
            this.FiveB = new System.Windows.Forms.Button();
            this.SixB = new System.Windows.Forms.Button();
            this.OneB = new System.Windows.Forms.Button();
            this.TwoB = new System.Windows.Forms.Button();
            this.ThreeB = new System.Windows.Forms.Button();
            this.ZeroB = new System.Windows.Forms.Button();
            this.DotB = new System.Windows.Forms.Button();
            this.SolveB = new System.Windows.Forms.Button();
            this.PlusB = new System.Windows.Forms.Button();
            this.MinusB = new System.Windows.Forms.Button();
            this.DivB = new System.Windows.Forms.Button();
            this.MultiB = new System.Windows.Forms.Button();
            this.PMB = new System.Windows.Forms.Button();
            this.RmB = new System.Windows.Forms.Button();
            this.SqrtB = new System.Windows.Forms.Button();
            this.SqrB = new System.Windows.Forms.Button();
            this.SqrtYB = new System.Windows.Forms.Button();
            this.DegB = new System.Windows.Forms.Button();
            this.ClearB = new System.Windows.Forms.Button();
            this.FacB = new System.Windows.Forms.Button();
            this.M_ClearB = new System.Windows.Forms.Button();
            this.M_SumB = new System.Windows.Forms.Button();
            this.M_SubB = new System.Windows.Forms.Button();
            this.M_DivB = new System.Windows.Forms.Button();
            this.M_MultB = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Comic Sans MS", 30F);
            this.textBox1.Location = new System.Drawing.Point(0, 0);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(506, 61);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "0";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SevenB
            // 
            this.SevenB.Font = new System.Drawing.Font("Rockwell Extra Bold", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SevenB.Location = new System.Drawing.Point(0, 134);
            this.SevenB.Name = "SevenB";
            this.SevenB.Size = new System.Drawing.Size(50, 50);
            this.SevenB.TabIndex = 1;
            this.SevenB.Text = "7";
            this.SevenB.UseVisualStyleBackColor = true;
            this.SevenB.Click += new System.EventHandler(this.SevenB_Click);
            // 
            // NineB
            // 
            this.NineB.Font = new System.Drawing.Font("Rockwell Extra Bold", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NineB.Location = new System.Drawing.Point(112, 134);
            this.NineB.Name = "NineB";
            this.NineB.Size = new System.Drawing.Size(50, 50);
            this.NineB.TabIndex = 2;
            this.NineB.Text = "9";
            this.NineB.UseVisualStyleBackColor = true;
            this.NineB.Click += new System.EventHandler(this.NineB_Click);
            // 
            // EightB
            // 
            this.EightB.Font = new System.Drawing.Font("Rockwell Extra Bold", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EightB.Location = new System.Drawing.Point(56, 134);
            this.EightB.Name = "EightB";
            this.EightB.Size = new System.Drawing.Size(50, 50);
            this.EightB.TabIndex = 3;
            this.EightB.Text = "8";
            this.EightB.UseVisualStyleBackColor = true;
            this.EightB.Click += new System.EventHandler(this.EightB_Click);
            // 
            // FourB
            // 
            this.FourB.Font = new System.Drawing.Font("Rockwell Extra Bold", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FourB.Location = new System.Drawing.Point(0, 190);
            this.FourB.Name = "FourB";
            this.FourB.Size = new System.Drawing.Size(50, 50);
            this.FourB.TabIndex = 4;
            this.FourB.Text = "4";
            this.FourB.UseVisualStyleBackColor = true;
            this.FourB.Click += new System.EventHandler(this.FourB_Click);
            // 
            // FiveB
            // 
            this.FiveB.Font = new System.Drawing.Font("Rockwell Extra Bold", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FiveB.Location = new System.Drawing.Point(56, 190);
            this.FiveB.Name = "FiveB";
            this.FiveB.Size = new System.Drawing.Size(50, 50);
            this.FiveB.TabIndex = 5;
            this.FiveB.Text = "5";
            this.FiveB.UseVisualStyleBackColor = true;
            this.FiveB.Click += new System.EventHandler(this.FiveB_Click);
            // 
            // SixB
            // 
            this.SixB.Font = new System.Drawing.Font("Rockwell Extra Bold", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SixB.Location = new System.Drawing.Point(112, 190);
            this.SixB.Name = "SixB";
            this.SixB.Size = new System.Drawing.Size(50, 50);
            this.SixB.TabIndex = 6;
            this.SixB.Text = "6";
            this.SixB.UseVisualStyleBackColor = true;
            this.SixB.Click += new System.EventHandler(this.SixB_Click);
            // 
            // OneB
            // 
            this.OneB.Font = new System.Drawing.Font("Rockwell Extra Bold", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OneB.Location = new System.Drawing.Point(0, 246);
            this.OneB.Name = "OneB";
            this.OneB.Size = new System.Drawing.Size(50, 50);
            this.OneB.TabIndex = 7;
            this.OneB.Text = "1";
            this.OneB.UseVisualStyleBackColor = true;
            this.OneB.Click += new System.EventHandler(this.OneB_Click);
            // 
            // TwoB
            // 
            this.TwoB.Font = new System.Drawing.Font("Rockwell Extra Bold", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TwoB.Location = new System.Drawing.Point(56, 246);
            this.TwoB.Name = "TwoB";
            this.TwoB.Size = new System.Drawing.Size(50, 50);
            this.TwoB.TabIndex = 8;
            this.TwoB.Text = "2";
            this.TwoB.UseVisualStyleBackColor = true;
            this.TwoB.Click += new System.EventHandler(this.TwoB_Click);
            // 
            // ThreeB
            // 
            this.ThreeB.Font = new System.Drawing.Font("Rockwell Extra Bold", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ThreeB.Location = new System.Drawing.Point(112, 246);
            this.ThreeB.Name = "ThreeB";
            this.ThreeB.Size = new System.Drawing.Size(50, 50);
            this.ThreeB.TabIndex = 9;
            this.ThreeB.Text = "3";
            this.ThreeB.UseVisualStyleBackColor = true;
            this.ThreeB.Click += new System.EventHandler(this.ThreeB_Click);
            // 
            // ZeroB
            // 
            this.ZeroB.Font = new System.Drawing.Font("Rockwell Extra Bold", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ZeroB.Location = new System.Drawing.Point(0, 302);
            this.ZeroB.Name = "ZeroB";
            this.ZeroB.Size = new System.Drawing.Size(106, 50);
            this.ZeroB.TabIndex = 10;
            this.ZeroB.Text = "0";
            this.ZeroB.UseVisualStyleBackColor = true;
            this.ZeroB.Click += new System.EventHandler(this.ZeroB_Click);
            // 
            // DotB
            // 
            this.DotB.Font = new System.Drawing.Font("Rockwell Extra Bold", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DotB.Location = new System.Drawing.Point(112, 302);
            this.DotB.Name = "DotB";
            this.DotB.Size = new System.Drawing.Size(50, 50);
            this.DotB.TabIndex = 11;
            this.DotB.Text = ",";
            this.DotB.UseVisualStyleBackColor = true;
            this.DotB.Click += new System.EventHandler(this.DotB_Click);
            // 
            // SolveB
            // 
            this.SolveB.Font = new System.Drawing.Font("Century", 20F, System.Drawing.FontStyle.Bold);
            this.SolveB.Location = new System.Drawing.Point(0, 358);
            this.SolveB.Name = "SolveB";
            this.SolveB.Size = new System.Drawing.Size(106, 50);
            this.SolveB.TabIndex = 12;
            this.SolveB.Text = "Solve";
            this.SolveB.UseVisualStyleBackColor = true;
            this.SolveB.Click += new System.EventHandler(this.SolveB_Click);
            // 
            // PlusB
            // 
            this.PlusB.Font = new System.Drawing.Font("Rockwell Extra Bold", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PlusB.Location = new System.Drawing.Point(168, 246);
            this.PlusB.Name = "PlusB";
            this.PlusB.Size = new System.Drawing.Size(50, 50);
            this.PlusB.TabIndex = 13;
            this.PlusB.Text = "+";
            this.PlusB.UseVisualStyleBackColor = true;
            this.PlusB.Click += new System.EventHandler(this.PlusB_Click);
            // 
            // MinusB
            // 
            this.MinusB.Font = new System.Drawing.Font("Rockwell Extra Bold", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MinusB.Location = new System.Drawing.Point(168, 190);
            this.MinusB.Name = "MinusB";
            this.MinusB.Size = new System.Drawing.Size(50, 50);
            this.MinusB.TabIndex = 14;
            this.MinusB.Text = "-";
            this.MinusB.UseVisualStyleBackColor = true;
            this.MinusB.Click += new System.EventHandler(this.MinusB_Click);
            // 
            // DivB
            // 
            this.DivB.Font = new System.Drawing.Font("Rockwell Extra Bold", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DivB.Location = new System.Drawing.Point(168, 134);
            this.DivB.Name = "DivB";
            this.DivB.Size = new System.Drawing.Size(50, 50);
            this.DivB.TabIndex = 15;
            this.DivB.Text = "/";
            this.DivB.UseVisualStyleBackColor = true;
            this.DivB.Click += new System.EventHandler(this.DivB_Click);
            // 
            // MultiB
            // 
            this.MultiB.Font = new System.Drawing.Font("Rockwell Extra Bold", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MultiB.Location = new System.Drawing.Point(168, 302);
            this.MultiB.Name = "MultiB";
            this.MultiB.Size = new System.Drawing.Size(50, 50);
            this.MultiB.TabIndex = 16;
            this.MultiB.Text = "*";
            this.MultiB.UseVisualStyleBackColor = true;
            this.MultiB.Click += new System.EventHandler(this.MultiB_Click);
            // 
            // PMB
            // 
            this.PMB.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F);
            this.PMB.Location = new System.Drawing.Point(0, 78);
            this.PMB.Name = "PMB";
            this.PMB.Size = new System.Drawing.Size(50, 50);
            this.PMB.TabIndex = 17;
            this.PMB.Text = "+/-";
            this.PMB.UseVisualStyleBackColor = true;
            this.PMB.Click += new System.EventHandler(this.PMB_Click);
            // 
            // RmB
            // 
            this.RmB.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F);
            this.RmB.Location = new System.Drawing.Point(56, 78);
            this.RmB.Name = "RmB";
            this.RmB.Size = new System.Drawing.Size(50, 50);
            this.RmB.TabIndex = 18;
            this.RmB.Text = "<--";
            this.RmB.UseVisualStyleBackColor = true;
            this.RmB.Click += new System.EventHandler(this.RmB_Click);
            // 
            // SqrtB
            // 
            this.SqrtB.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F);
            this.SqrtB.Location = new System.Drawing.Point(224, 134);
            this.SqrtB.Name = "SqrtB";
            this.SqrtB.Size = new System.Drawing.Size(50, 50);
            this.SqrtB.TabIndex = 19;
            this.SqrtB.Text = "√";
            this.SqrtB.UseVisualStyleBackColor = true;
            this.SqrtB.Click += new System.EventHandler(this.SqrtB_Click);
            // 
            // SqrB
            // 
            this.SqrB.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F);
            this.SqrB.Location = new System.Drawing.Point(224, 190);
            this.SqrB.Name = "SqrB";
            this.SqrB.Size = new System.Drawing.Size(50, 50);
            this.SqrB.TabIndex = 20;
            this.SqrB.Text = "x^2";
            this.SqrB.UseVisualStyleBackColor = true;
            this.SqrB.Click += new System.EventHandler(this.SqrB_Click);
            // 
            // SqrtYB
            // 
            this.SqrtYB.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F);
            this.SqrtYB.Location = new System.Drawing.Point(224, 246);
            this.SqrtYB.Name = "SqrtYB";
            this.SqrtYB.Size = new System.Drawing.Size(50, 50);
            this.SqrtYB.TabIndex = 21;
            this.SqrtYB.Text = "x√";
            this.SqrtYB.UseVisualStyleBackColor = true;
            this.SqrtYB.Click += new System.EventHandler(this.SqrtYB_Click);
            // 
            // DegB
            // 
            this.DegB.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F);
            this.DegB.Location = new System.Drawing.Point(224, 302);
            this.DegB.Name = "DegB";
            this.DegB.Size = new System.Drawing.Size(50, 50);
            this.DegB.TabIndex = 22;
            this.DegB.Text = "x^y";
            this.DegB.UseVisualStyleBackColor = true;
            this.DegB.Click += new System.EventHandler(this.DegB_Click);
            // 
            // ClearB
            // 
            this.ClearB.Font = new System.Drawing.Font("Century", 20F, System.Drawing.FontStyle.Bold);
            this.ClearB.Location = new System.Drawing.Point(112, 358);
            this.ClearB.Name = "ClearB";
            this.ClearB.Size = new System.Drawing.Size(106, 50);
            this.ClearB.TabIndex = 23;
            this.ClearB.Text = "Clear";
            this.ClearB.UseVisualStyleBackColor = true;
            this.ClearB.Click += new System.EventHandler(this.ClearB_Click);
            // 
            // FacB
            // 
            this.FacB.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F);
            this.FacB.Location = new System.Drawing.Point(224, 358);
            this.FacB.Name = "FacB";
            this.FacB.Size = new System.Drawing.Size(50, 50);
            this.FacB.TabIndex = 24;
            this.FacB.Text = "x!";
            this.FacB.UseVisualStyleBackColor = true;
            this.FacB.Click += new System.EventHandler(this.FacB_Click);
            // 
            // M_ClearB
            // 
            this.M_ClearB.Font = new System.Drawing.Font("Century", 20F, System.Drawing.FontStyle.Bold);
            this.M_ClearB.Location = new System.Drawing.Point(280, 134);
            this.M_ClearB.Name = "M_ClearB";
            this.M_ClearB.Size = new System.Drawing.Size(106, 50);
            this.M_ClearB.TabIndex = 25;
            this.M_ClearB.Text = "MRC";
            this.M_ClearB.UseVisualStyleBackColor = true;
            this.M_ClearB.Click += new System.EventHandler(this.M_ClearB_Click);
            // 
            // M_SumB
            // 
            this.M_SumB.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F);
            this.M_SumB.Location = new System.Drawing.Point(280, 190);
            this.M_SumB.Name = "M_SumB";
            this.M_SumB.Size = new System.Drawing.Size(50, 50);
            this.M_SumB.TabIndex = 26;
            this.M_SumB.Text = "M+";
            this.M_SumB.UseVisualStyleBackColor = true;
            this.M_SumB.Click += new System.EventHandler(this.M_SumB_Click);
            // 
            // M_SubB
            // 
            this.M_SubB.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F);
            this.M_SubB.Location = new System.Drawing.Point(336, 190);
            this.M_SubB.Name = "M_SubB";
            this.M_SubB.Size = new System.Drawing.Size(50, 50);
            this.M_SubB.TabIndex = 27;
            this.M_SubB.Text = "M-";
            this.M_SubB.UseVisualStyleBackColor = true;
            this.M_SubB.Click += new System.EventHandler(this.M_SubB_Click);
            // 
            // M_DivB
            // 
            this.M_DivB.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F);
            this.M_DivB.Location = new System.Drawing.Point(280, 246);
            this.M_DivB.Name = "M_DivB";
            this.M_DivB.Size = new System.Drawing.Size(50, 50);
            this.M_DivB.TabIndex = 28;
            this.M_DivB.Text = "M /";
            this.M_DivB.UseVisualStyleBackColor = true;
            this.M_DivB.Click += new System.EventHandler(this.M_DivB_Click);
            // 
            // M_MultB
            // 
            this.M_MultB.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F);
            this.M_MultB.Location = new System.Drawing.Point(336, 246);
            this.M_MultB.Name = "M_MultB";
            this.M_MultB.Size = new System.Drawing.Size(50, 50);
            this.M_MultB.TabIndex = 29;
            this.M_MultB.Text = "M *";
            this.M_MultB.UseVisualStyleBackColor = true;
            this.M_MultB.Click += new System.EventHandler(this.M_MultB_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 474);
            this.Controls.Add(this.M_MultB);
            this.Controls.Add(this.M_DivB);
            this.Controls.Add(this.M_SubB);
            this.Controls.Add(this.M_SumB);
            this.Controls.Add(this.M_ClearB);
            this.Controls.Add(this.FacB);
            this.Controls.Add(this.ClearB);
            this.Controls.Add(this.DegB);
            this.Controls.Add(this.SqrtYB);
            this.Controls.Add(this.SqrB);
            this.Controls.Add(this.SqrtB);
            this.Controls.Add(this.RmB);
            this.Controls.Add(this.PMB);
            this.Controls.Add(this.MultiB);
            this.Controls.Add(this.DivB);
            this.Controls.Add(this.MinusB);
            this.Controls.Add(this.PlusB);
            this.Controls.Add(this.SolveB);
            this.Controls.Add(this.DotB);
            this.Controls.Add(this.ZeroB);
            this.Controls.Add(this.ThreeB);
            this.Controls.Add(this.TwoB);
            this.Controls.Add(this.OneB);
            this.Controls.Add(this.SixB);
            this.Controls.Add(this.FiveB);
            this.Controls.Add(this.FourB);
            this.Controls.Add(this.EightB);
            this.Controls.Add(this.NineB);
            this.Controls.Add(this.SevenB);
            this.Controls.Add(this.textBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form1";
            this.Text = "LimboCalc";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button SevenB;
        private System.Windows.Forms.Button NineB;
        private System.Windows.Forms.Button EightB;
        private System.Windows.Forms.Button FourB;
        private System.Windows.Forms.Button FiveB;
        private System.Windows.Forms.Button SixB;
        private System.Windows.Forms.Button OneB;
        private System.Windows.Forms.Button TwoB;
        private System.Windows.Forms.Button ThreeB;
        private System.Windows.Forms.Button ZeroB;
        private System.Windows.Forms.Button DotB;
        private System.Windows.Forms.Button SolveB;
        private System.Windows.Forms.Button PlusB;
        private System.Windows.Forms.Button MinusB;
        private System.Windows.Forms.Button DivB;
        private System.Windows.Forms.Button MultiB;
        private System.Windows.Forms.Button PMB;
        private System.Windows.Forms.Button RmB;
        private System.Windows.Forms.Button SqrtB;
        private System.Windows.Forms.Button SqrB;
        private System.Windows.Forms.Button SqrtYB;
        private System.Windows.Forms.Button DegB;
        private System.Windows.Forms.Button ClearB;
        private System.Windows.Forms.Button FacB;
        private System.Windows.Forms.Button M_ClearB;
        private System.Windows.Forms.Button M_SumB;
        private System.Windows.Forms.Button M_SubB;
        private System.Windows.Forms.Button M_DivB;
        private System.Windows.Forms.Button M_MultB;
    }
}

