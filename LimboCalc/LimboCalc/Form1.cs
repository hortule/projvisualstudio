﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace LimboCalc
{
    public partial class Form1 : Form
    {
        Calc calc;

        public Form1()
        {
            InitializeComponent();
            calc = new Calc();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void SolveB_Click(object sender, EventArgs e)
        {
            if (!MultiB.Enabled)
            {
                textBox1.Text = calc.Mult(Convert.ToDouble(textBox1.Text)).ToString();
            }

            if (!DivB.Enabled)
            {
                textBox1.Text = calc.Div(Convert.ToDouble(textBox1.Text)).ToString();
            }

            if (!PlusB.Enabled)
            {
                textBox1.Text = calc.Sum(Convert.ToDouble(textBox1.Text)).ToString();
            }

            if (!MinusB.Enabled)
            {
                textBox1.Text = calc.Sub(Convert.ToDouble(textBox1.Text)).ToString();
            }

            if (!SqrtYB.Enabled)
            {
                textBox1.Text = calc.SqrtY(Convert.ToDouble(textBox1.Text)).ToString();
            }

            if (!DegB.Enabled)
            {
                textBox1.Text = calc.DegreeY(Convert.ToDouble(textBox1.Text)).ToString();
            }
            calc.Clear_X();
            FreeButtons();        
            counter = 0;
        }

        private void SevenB_Click(object sender, EventArgs e)
        {
            textBox1.Text += "7";
            CorrectNumber();
        }

        private void EightB_Click(object sender, EventArgs e)
        {
            textBox1.Text += "8";
            CorrectNumber();
        }

        private void NineB_Click(object sender, EventArgs e)
        {
            textBox1.Text += "9";
            CorrectNumber();
        }

        private void FourB_Click(object sender, EventArgs e)
        {
            textBox1.Text += "4";
            CorrectNumber();
        }

        private void FiveB_Click(object sender, EventArgs e)
        {
            textBox1.Text += "5";
            CorrectNumber();
        }

        private void SixB_Click(object sender, EventArgs e)
        {
            textBox1.Text += "6";
            CorrectNumber();
        }

        private void OneB_Click(object sender, EventArgs e)
        {
            textBox1.Text += "1";
            CorrectNumber();
        }

        private void TwoB_Click(object sender, EventArgs e)
        {
            textBox1.Text += "2";
            CorrectNumber();
        }

        private void ThreeB_Click(object sender, EventArgs e)
        {
            textBox1.Text += "3";
            CorrectNumber();
        }

        private void ZeroB_Click(object sender, EventArgs e)
        {
            textBox1.Text += "0";
            CorrectNumber();
        }

        private void DotB_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text.IndexOf(",") == -1) && (textBox1.Text.IndexOf("не") == -1) && (textBox1.Text.IndexOf("∞") == -1))
            {
                textBox1.Text += ",";
                CorrectNumber();
            }
            counter = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void CorrectNumber()
        {
            counter = 0;
            //If error catched writing permission denied
            if (textBox1.Text.IndexOf("не") != -1)
            {
                textBox1.Text = textBox1.Text.Substring(0, textBox1.Text.Length - 1);
            }

            if (textBox1.Text.IndexOf("∞") != -1)
            {
                textBox1.Text = textBox1.Text.Substring(0, textBox1.Text.Length - 1);
            }

            //Removing of insignificant zero
            if (textBox1.Text[0] == '0' && (textBox1.Text.IndexOf(",") != 1))
            {
                textBox1.Text = textBox1.Text.Remove(0, 1);
            }

            //Removing of insignificant zero in negative numbers
            if (textBox1.Text[0] == '-')
            {
                if (textBox1.Text[1] == '0' && (textBox1.Text.IndexOf(",") != 2))
                {
                    textBox1.Text = textBox1.Text.Remove(1, 1);
                }
            }
        }

        //check of button what needed second argument
        private bool CanPress()
        {
            if (!MultiB.Enabled)
                return false;

            if (!DivB.Enabled)
                return false;

            if (!PlusB.Enabled)
                return false;

            if (!MinusB.Enabled)
                return false;

            if (!SqrtYB.Enabled)
                return false;

            if (!DegB.Enabled)
                return false;

            return true;
        }

        //make buttons what needed second argument free
        private void FreeButtons()
        {
            MultiB.Enabled = true;
            DivB.Enabled = true;
            PlusB.Enabled = true;
            MinusB.Enabled = true;
            SqrtYB.Enabled = true;
            DegB.Enabled = true;
        }

        //counter of MRC taps
        int counter;

        private void M_ClearB_Click(object sender, EventArgs e)
        {
            if (CanPress())
            {
                counter ++;
                if (counter == 1)
                    textBox1.Text = calc.MemoryShow().ToString();
                if (counter == 2)
                {
                    calc.Memory_Clear();
                    textBox1.Text = "0";
                    counter = 0;
                }
            }
        }

        private void ClearB_Click(object sender, EventArgs e)
        {
            textBox1.Text = "0";
            calc.Clear_X();
            FreeButtons();
            counter = 0;
        }

        private void PMB_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text[0] == '-' ? textBox1.Text.Remove(0, 1) : "-" + textBox1.Text;
        }

        private void MultiB_Click(object sender, EventArgs e)
        {
            if (CanPress())
            {
                calc.Read_X(Convert.ToDouble(textBox1.Text));
                MultiB.Enabled = false;
                textBox1.Text = "0";
            }
            counter = 0;
        }

        private void DivB_Click(object sender, EventArgs e)
        {
            if (CanPress())
            {
                calc.Read_X(Convert.ToDouble(textBox1.Text));
                DivB.Enabled = false;
                textBox1.Text = "0";
            }
            counter = 0;
        }

        private void PlusB_Click(object sender, EventArgs e)
        {
            if (CanPress())
            {
                calc.Read_X(Convert.ToDouble(textBox1.Text));
                PlusB.Enabled = false;
                textBox1.Text = "0";
            }
            counter = 0;
        }

        private void MinusB_Click(object sender, EventArgs e)
        {
            if (CanPress())
            {
                calc.Read_X(Convert.ToDouble(textBox1.Text));
                MinusB.Enabled = false;
                textBox1.Text = "0";
            }
            counter = 0;
        }

        private void SqrtYB_Click(object sender, EventArgs e)
        {
            if (CanPress())
            {
                calc.Read_X(Convert.ToDouble(textBox1.Text));
                SqrtYB.Enabled = false;
                textBox1.Text = "0";
            }
            counter = 0;
        }

        private void SqrB_Click(object sender, EventArgs e)
        {
            if (CanPress())
            {
                calc.Read_X(Convert.ToDouble(textBox1.Text));
                textBox1.Text = calc.Square().ToString();
                calc.Clear_X();
                FreeButtons();
            }
        }

        private void SqrtB_Click(object sender, EventArgs e)
        {
            if (CanPress())
            {
                calc.Read_X(Convert.ToDouble(textBox1.Text));
                textBox1.Text = calc.Sqrt().ToString();
                calc.Clear_X();
                FreeButtons();
            }
            counter = 0;
        }

        private void DegB_Click(object sender, EventArgs e)
        {
            if (CanPress())
            {
                calc.Read_X(Convert.ToDouble(textBox1.Text));
                DegB.Enabled = false;
                textBox1.Text = "0";
            }
            counter = 0;
        }

        private void FacB_Click(object sender, EventArgs e)
        {
            if (CanPress())
            {
                calc.Read_X(Convert.ToDouble(textBox1.Text));
                textBox1.Text = calc.Factorial().ToString();
                calc.Clear_X();
                FreeButtons();
            }
            counter = 0;
        }

        private void M_DivB_Click(object sender, EventArgs e)
        {
            calc.M_Division(Convert.ToDouble(textBox1.Text));
            counter = 0;
        }

        private void M_MultB_Click(object sender, EventArgs e)
        {
            calc.M_Mult(Convert.ToDouble(textBox1.Text));
            counter = 0;
        }

        private void M_SumB_Click(object sender, EventArgs e)
        {
            calc.M_Sum(Convert.ToDouble(textBox1.Text));
            counter = 0;
        }

        private void M_SubB_Click(object sender, EventArgs e)
        {
            calc.M_Sub(Convert.ToDouble(textBox1.Text));
            counter = 0;
        }

        private void RmB_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text.Remove(textBox1.TextLength-1, 1);
            if (textBox1.Text == "")
            {
                textBox1.Text = "0";
            }
        }
    }

}

