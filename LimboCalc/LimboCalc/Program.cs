﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LimboCalc
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}


public interface LimboCalcInterface
{
    //read x
    void Read_X(double x);

    //clear x
    void Clear_X();

    //multiplication
    double Mult(double y);

    //division
    double Div(double y);

    double Sum(double y);

    //subtraction
    double Sub(double y);

    //x root 
    double SqrtY(double y);

    //y degree
    double DegreeY(double y);

    //sqrt root
    double Sqrt();

    //second degree
    double Square();

    double Factorial();

    double MemoryShow();

    void Memory_Clear();

    //memory multiplication
    void M_Mult(double y);

    //memory division
    void M_Division(double y);

    //memory sum
    void M_Sum(double y);

    //memory subtraction
    void M_Sub(double y);
}

public class Calc : LimboCalcInterface
{
    //x variable of calc
    private double x = 0;

    //memory of calc
    private double mem = 0;

    public void Read_X(double x)
    {
        this.x = x;
    }

    public void Clear_X()
    {
        x = 0;
    }

    public double Mult(double y)
    {
        return x * y;
    }

    public double Div(double y)
    {
        return x / y;
    }

    public double Sum(double y)
    {
        return x + y;
    }

    public double Sub(double y)
    {
        return x - y;
    }

    public double SqrtY(double y)
    {
        return Math.Pow(x, 1 / y);
    }

    public double DegreeY(double y)
    {
        return Math.Pow(x, y);
    }

    public double Sqrt()
    {
        return Math.Sqrt(x);
    }

    public double Square()
    {
        return x * x;
    }

    public double Factorial()
    {
        double fact = 1;
        for (int i = 2; i <= x; i++)
        {
            fact *= i;
        }
        return fact;
    }

    public double MemoryShow() => mem;

    public void Memory_Clear() => mem = 0;

    public void M_Mult(double y) => mem *= y;

    public void M_Division(double y) => mem /= y;

    public void M_Sum(double y) => mem += y;

    public void M_Sub(double y) => mem -= y;
}
