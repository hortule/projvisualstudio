﻿namespace Poesia
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.BtZoomOut = new System.Windows.Forms.ToolStripButton();
            this.BtZoomIn = new System.Windows.Forms.ToolStripButton();
            this.BtAbout = new System.Windows.Forms.ToolStripButton();
            this.tc = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.meThroughLine = new System.Windows.Forms.TextBox();
            this.meThrought = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.meFirstWord = new System.Windows.Forms.TextBox();
            this.meFirstWords = new System.Windows.Forms.TextBox();
            this.toolStrip1.SuspendLayout();
            this.tc.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtZoomOut,
            this.BtZoomIn,
            this.BtAbout});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(358, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // BtZoomOut
            // 
            this.BtZoomOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.BtZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("BtZoomOut.Image")));
            this.BtZoomOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtZoomOut.Name = "BtZoomOut";
            this.BtZoomOut.Size = new System.Drawing.Size(66, 22);
            this.BtZoomOut.Text = "Zoom Out";
            // 
            // BtZoomIn
            // 
            this.BtZoomIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.BtZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("BtZoomIn.Image")));
            this.BtZoomIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtZoomIn.Name = "BtZoomIn";
            this.BtZoomIn.Size = new System.Drawing.Size(56, 22);
            this.BtZoomIn.Text = "Zoom In";
            // 
            // BtAbout
            // 
            this.BtAbout.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.BtAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.BtAbout.Image = ((System.Drawing.Image)(resources.GetObject("BtAbout.Image")));
            this.BtAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BtAbout.Name = "BtAbout";
            this.BtAbout.Size = new System.Drawing.Size(44, 22);
            this.BtAbout.Text = "About";
            // 
            // tc
            // 
            this.tc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tc.Controls.Add(this.tabPage1);
            this.tc.Controls.Add(this.tabPage2);
            this.tc.Controls.Add(this.tabPage3);
            this.tc.Location = new System.Drawing.Point(0, 28);
            this.tc.Name = "tc";
            this.tc.SelectedIndex = 0;
            this.tc.Size = new System.Drawing.Size(358, 280);
            this.tc.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(350, 254);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Полностью";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(3, 3);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(344, 248);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Под свист розги,\r\nпод поповское пение,\r\nрабом жила российская паства.\r\nЭто называ" +
    "лось: единение.\r\nцеркви и государства. ";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.meThroughLine);
            this.tabPage2.Controls.Add(this.meThrought);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(350, 254);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Через строку";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // meThroughLine
            // 
            this.meThroughLine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meThroughLine.Location = new System.Drawing.Point(3, 3);
            this.meThroughLine.Multiline = true;
            this.meThroughLine.Name = "meThroughLine";
            this.meThroughLine.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.meThroughLine.Size = new System.Drawing.Size(344, 248);
            this.meThroughLine.TabIndex = 1;
            this.meThroughLine.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // meThrought
            // 
            this.meThrought.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meThrought.Location = new System.Drawing.Point(3, 3);
            this.meThrought.Multiline = true;
            this.meThrought.Name = "meThrought";
            this.meThrought.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.meThrought.Size = new System.Drawing.Size(344, 248);
            this.meThrought.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.meFirstWord);
            this.tabPage3.Controls.Add(this.meFirstWords);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(350, 254);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Первые слова";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // meFirstWord
            // 
            this.meFirstWord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meFirstWord.Location = new System.Drawing.Point(3, 3);
            this.meFirstWord.Multiline = true;
            this.meFirstWord.Name = "meFirstWord";
            this.meFirstWord.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.meFirstWord.Size = new System.Drawing.Size(344, 248);
            this.meFirstWord.TabIndex = 1;
            this.meFirstWord.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // meFirstWords
            // 
            this.meFirstWords.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meFirstWords.Location = new System.Drawing.Point(3, 3);
            this.meFirstWords.Multiline = true;
            this.meFirstWords.Name = "meFirstWords";
            this.meFirstWords.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.meFirstWords.Size = new System.Drawing.Size(344, 248);
            this.meFirstWords.TabIndex = 0;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 311);
            this.Controls.Add(this.tc);
            this.Controls.Add(this.toolStrip1);
            this.Name = "fm";
            this.Text = "Form1";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tc.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton BtZoomOut;
        private System.Windows.Forms.ToolStripButton BtZoomIn;
        private System.Windows.Forms.ToolStripButton BtAbout;
        private System.Windows.Forms.TabControl tc;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox meThrought;
        private System.Windows.Forms.TextBox meFirstWords;
        private System.Windows.Forms.TextBox meThroughLine;
        private System.Windows.Forms.TextBox meFirstWord;
    }
}

