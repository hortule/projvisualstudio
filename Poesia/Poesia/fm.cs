﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Poesia
{
    public partial class fm : Form
    {
        public fm()
        {
            //
            InitializeComponent();
            tc.TabIndex = 0;
            meThroughLine.Lines = UtilsStr.ThroughLine(textBox1.Lines);
            meFirstWord.Lines = UtilsStr.FirstWords(textBox1.Lines);
            //
            BtZoomOut.Click += BtZoomOut_Click;
            BtZoomIn.Click += BtZoomIn_Click;
            BtAbout.Click += BtAbout_Click;

        }

        private void BtAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Я сделал");
        }

        private void BtZoomIn_Click(object sender, EventArgs e)
        {
            float x = textBox1.Font.Size;
            x += 1;
            textBox1.Font = new Font(textBox1.Font.FontFamily, x);
        }

        private void BtZoomOut_Click(object sender, EventArgs e)
        {
            float x = textBox1.Font.Size;
            x -= 1;
            textBox1.Font = new Font(textBox1.Font.FontFamily, x);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
