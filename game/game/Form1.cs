﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace game
{
    public partial class FormGame : Form
    {
        private readonly Graphics panel;
        private readonly Random rnd = new Random();

        private List<Img> elems = new List<Img>();
        private int score = 0;

        public FormGame()
        {
            InitializeComponent();

            panel = this.CreateGraphics();
        }

        private void FormMain_KeyUp(object sender, KeyEventArgs key)
        {
            if (key.KeyCode == Keys.Escape)
                this.Close();
        }

        private void timerFPS_Tick(object sender, EventArgs e)
        {
            panel.Clear(this.BackColor);

            if (elems.Count < 4)
                elems.Add(new Img(rnd.Next(this.Width - 200), 0, 200, 200));

            foreach (Img elem in elems)
            {
                if (elem.move(this.Height))
                {
                    elems.Remove(elem);
                    score--;
                    break;
                }
                elem.draw(panel);
            }

            label1.Text = "Счёт: " + score.ToString();
        }

        private void FormMain_MouseDown(object sender, MouseEventArgs e)
        {
            foreach (Img elem in elems)
            {
                if (elem.clicked(Cursor.Position.X, Cursor.Position.Y))
                {
                    elems.Remove(elem);
                    score++;
                    break;
                }
            }
        }
    }
}
