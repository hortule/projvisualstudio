﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game
{
    class Img
    {
        private readonly int x, width, height, ySpeed;
        private readonly Random rnd = new Random();

        private int y;

        public Img(int x, int y, int width, int height)
        {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;

            ySpeed = rnd.Next(10, 20);
        }

        public bool move(int maxY)
        {
            y += ySpeed;

            if (y >= maxY)
                return true;
            else
                return false;
        }

        public bool clicked(int x, int y)
        {
            if ((x >= this.x && x <= this.x + width) && (y >= this.y && y <= this.y + height))
                return true;
            else
                return false;
        }

        public void draw(Graphics panel)
        {
            panel.DrawImage(Properties.Resources.ball, x, y, width, height);
        }
    }
}
