﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labFinGreed
{
    public partial class Auth : Form
    {
        public Auth()
        {
            InitializeComponent();
        }

        private void Auth_Load(object sender, EventArgs e)
        {

        }

        public string name;

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            RL rl = new RL
            {
                Owner = this
            };

            name = rl.Log(loginEdit.Text, passEdit.Text);

            if (name != null)
            {
                Main main = new Main
                {
                    Owner = this                    
                };
                main.name = name;          
                main.Show();
                Hide();
            }             
        }

        private void regButton_Click(object sender, EventArgs e)
        {
            RL rl = new RL
            {
                Owner = this
            };

            rl.Reg(loginEdit.Text, passEdit.Text);

        }
    }

    public class Hash
    {
        public string getHashSha256(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }
    }

    public class RL : Form
    {
        public void Reg(string login, string pass)
        {
            if (login != "" && pass != "")
            {

                Hash hash = new Hash();
                var ph = hash.getHashSha256(pass);

                StreamReader sr = new StreamReader("authd.csv", Encoding.Default);
                string s = sr.ReadLine();

                while (s != null)
                {
                    var r = s.Split(';');

                    if (r[0].ToLower() == login.ToLower())
                    {
                        MessageBox.Show("already registered");
                        sr.Close();
                        return;
                    }

                    s = sr.ReadLine();
                }
                sr.Close();

                StreamWriter sw = new StreamWriter("authd.csv", true, Encoding.Default);
                sw.WriteLine(login.ToLower() + ";" + ph);
                sw.Close();

                MessageBox.Show("Succesfuly registred");
            }
        }

        public string Log(string login, string pass)
        {            
            Hash hash = new Hash();
            var ph = hash.getHashSha256(pass);

            StreamReader sr = new StreamReader("authd.csv", Encoding.Default);
            string s = sr.ReadLine();

            while (s != null)
            {
                var r = s.Split(';');

                if (r[0].ToLower() == login.ToLower())
                {
                    if (r[1] == ph)
                    {
                        sr.Close();
                        return login;
                    }
                }

                s = sr.ReadLine();
            }
            sr.Close();
            MessageBox.Show("Not registered");
            return null;
        }
    }

    
}
