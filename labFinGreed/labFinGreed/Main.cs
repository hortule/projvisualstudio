﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labFinGreed
{
    

    public partial class Main : Form
    {
        
        public Main()
        {
            InitializeComponent();            
        }

        private void changeButton_Click(object sender, EventArgs e)
        {
            BalanceParams balanceparams = new BalanceParams()
            {
                Owner = this
            };
            balanceparams.ShowDialog();
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Operations operations = new Operations();
            operations.Owner = this;
            operations.ShowDialog();
            dateselect_SelectedIndexChanged(dateselect, e);
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Oper oper = new Oper();
            oper.Owner = this;
            oper.Save();
            Application.Exit();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            Oper oper = new Oper();
            oper.Owner = this;
            oper.Load_osn();

            oper.upd();
        }

        public void Main_Activated(object sender, EventArgs e)
        {
            Oper oper = new Oper();
            oper.Owner = this;

            int balance = 0, pd = 0;
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {   
                if (dataGridView1[0, i].Value.ToString() == "Доход")
                {
                    balance += Convert.ToInt32(dataGridView1[2, i].Value);
                }
                else
                    balance -= Convert.ToInt32(dataGridView1[2, i].Value);
            }
            int inuse = oper.Read_inuse();

            pd = oper.Pd(balance, inuse);
            balanceLable.Text = balance.ToString();
            spentsLable.Text = pd.ToString();
            depositeLable.Text = inuse.ToString();

            int income = 0, consumption = 0;

            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                if (dataGridView1[0, i].Value.ToString() == "Доход")
                {
                    income += Convert.ToInt32(dataGridView1[2, i].Value);
                }

                if (dataGridView1[0, i].Value.ToString() == "Расход")
                {
                    consumption += Convert.ToInt32(dataGridView1[2, i].Value);
                }

                chart1.Series[0].Points.Clear();
                chart1.Series[1].Points.Clear();
                chart1.Series[0].Points.AddXY(0, income);
                chart1.Series[1].Points.AddXY(1, consumption);
                chart1.Series[0].AxisLabel = "income";
                chart1.Series[1].AxisLabel = "outgo";
            }


        }

        private void dataGridView1_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            Oper oper = new Oper();
            oper.Owner = this;

            int income = 0, consumption = 0;

            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                if (dataGridView1[0, i].Value.ToString() == "Доход")
                {
                    income += Convert.ToInt32(dataGridView1[2, i].Value);
                }

                if (dataGridView1[0, i].Value.ToString() == "Расход")
                {
                    consumption += Convert.ToInt32(dataGridView1[2, i].Value);
                }
                chart1.Series[0].Points.Clear();
                chart1.Series[1].Points.Clear();
                chart1.Series[0].Points.AddXY(0, income);
                chart1.Series[1].Points.AddXY(1, consumption);
                chart1.Series[0].AxisLabel = "income";
                chart1.Series[1].AxisLabel = "outgo";
            }
            dateselect_SelectedIndexChanged(dateselect, e);
            oper.upd();
            Main_Activated(dataGridView1, e);
        }

        public void dateselect_SelectedIndexChanged(object sender, EventArgs e)
        {
            string date;
            try
            {
                date = dateselect.SelectedItem.ToString();
            }
            catch
            {
                return;
            }

            date = dateselect.SelectedItem.ToString();

            int income = 0, consumption = 0;

            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                if (dataGridView1[0, i].Value.ToString() == "Доход" && date == dataGridView1[3, i].Value.ToString())
                {
                    income += Convert.ToInt32(dataGridView1[2, i].Value);
                }

                if (dataGridView1[0, i].Value.ToString() == "Расход" && date == dataGridView1[3, i].Value.ToString())
                {
                    consumption += Convert.ToInt32(dataGridView1[2, i].Value);
                }
            }
            label4.Text = income.ToString();
            label7.Text = consumption.ToString();
        }

        public string name;


        public class Oper : Form
        {
            
            public void upd()
            {
                Main main = this.Owner as Main;
                int income = 0, consumption = 0;

                for (int i = 0; i < main.dataGridView1.RowCount; i++)
                {
                    if (main.dataGridView1[0, i].Value.ToString() == "Доход")
                    {
                        income += Convert.ToInt32(main.dataGridView1[2, i].Value);
                    }

                    if (main.dataGridView1[0, i].Value.ToString() == "Расход")
                    {
                        consumption += Convert.ToInt32(main.dataGridView1[2, i].Value);
                    }
                    if (main.dateselect.Items.Contains(main.dataGridView1[3, i].Value.ToString()) == false)
                    {
                        main.dateselect.Items.Add(main.dataGridView1[3, i].Value.ToString());
                    }
                }
                main.chart1.Series[0].Points.AddXY(0, income);
                main.chart1.Series[1].Points.AddXY(1, consumption);
                main.chart1.Series[0].AxisLabel = "income";
                main.chart1.Series[1].AxisLabel = "outgo";
            }

            public void Save()
            {
                Main main = this.Owner as Main;
                Auth auth = this.Owner as Auth;

                string name = main.name;
                string s = "";
                for (int i = 0; i < main.dataGridView1.RowCount; i++)
                {
                    s += main.dataGridView1[0, i].Value + ";" + main.dataGridView1[1, i].Value + ";" 
                        + main.dataGridView1[2, i].Value + ";" + main.dataGridView1[3, i].Value + "\n";
                }
                StreamWriter sw = new StreamWriter(name + ".csv", false, Encoding.Default);
                sw.Write(s);
                sw.Close();
            }

            public void Load_osn()
            {
                Main main = this.Owner as Main;

                string name = main.name;

                FileStream fs = new FileStream(name + ".csv", FileMode.OpenOrCreate);
                StreamReader sr = new StreamReader(fs, Encoding.Default);
                string s = sr.ReadLine();
                while (s != null)
                {
                    var r = s.Split(';');
                    main.dataGridView1.Rows.Add(r[0], r[1], r[2], r[3]);
                    s = sr.ReadLine();
                }
                fs.Close();
                sr.Close();
            }


            public int Read_inuse()
            {
                Auth auth = this.Owner as Auth;
                Main main = this.Owner as Main;

                string name = main.name + "1";
                FileStream fs = new FileStream(name + ".csv", FileMode.OpenOrCreate);
                StreamReader sr = new StreamReader(fs, Encoding.Default);
                int inuse = Convert.ToInt32(sr.ReadLine());
                sr.Close();
                return inuse;
            }

            public int Pd(int balance, int inuse)
            {
                Main main = this.Owner as Main;

                int free = balance - inuse;
                main.freeLable.Text = free.ToString();
                return free / (DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) - DateTime.Now.Day);
            }
        }

        
    }
}
