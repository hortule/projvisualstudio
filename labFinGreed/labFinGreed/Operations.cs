﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labFinGreed
{
    public partial class Operations : Form
    {
        public Operations()
        {
            InitializeComponent();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            Main main = this.Owner as Main;
            try
            {
                Convert.ToInt32(sumBox.Text);
            }
            catch
            {
                MessageBox.Show("Check sum text");
                return;
            }

            if (typeBox.Text != "")
            {
                string date = DateTime.Now.Day.ToString() + "." + DateTime.Now.Month.ToString() + "." + DateTime.Now.Year.ToString();
                main.dataGridView1.Rows.Add(typeBox.Text, categoryBox.Text, sumBox.Text, date);
            }
            else
                MessageBox.Show("Select type");

        }

        private void Operations_Load(object sender, EventArgs e)
        {
            Main main = this.Owner as Main;

            for (int i = 0; i < main.dataGridView1.RowCount; i++)
            {   if (!categoryBox.Items.Contains(main.dataGridView1[1, i].Value.ToString()))
                {
                    categoryBox.Items.Add(main.dataGridView1[1, i].Value.ToString());
                }
            }
        }

        private void sumBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void categoryBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void typeBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
