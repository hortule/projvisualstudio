﻿namespace labGenPassword
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buPassword = new System.Windows.Forms.Button();
            this.ckLower = new System.Windows.Forms.CheckBox();
            this.ckUpper = new System.Windows.Forms.CheckBox();
            this.ckSpec = new System.Windows.Forms.CheckBox();
            this.ckNumber = new System.Windows.Forms.CheckBox();
            this.edLenght = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.edLenght)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(12, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(373, 44);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "<Password>";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buPassword
            // 
            this.buPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buPassword.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.buPassword.Location = new System.Drawing.Point(12, 62);
            this.buPassword.Name = "buPassword";
            this.buPassword.Size = new System.Drawing.Size(373, 59);
            this.buPassword.TabIndex = 1;
            this.buPassword.Text = "Генерировать";
            this.buPassword.UseVisualStyleBackColor = true;
            this.buPassword.Click += new System.EventHandler(this.buPassword_Click);
            // 
            // ckLower
            // 
            this.ckLower.AutoSize = true;
            this.ckLower.Location = new System.Drawing.Point(12, 146);
            this.ckLower.Name = "ckLower";
            this.ckLower.Size = new System.Drawing.Size(174, 17);
            this.ckLower.TabIndex = 2;
            this.ckLower.Text = "Символы в нижнем регистре";
            this.ckLower.UseVisualStyleBackColor = true;
            // 
            // ckUpper
            // 
            this.ckUpper.AutoSize = true;
            this.ckUpper.Location = new System.Drawing.Point(12, 169);
            this.ckUpper.Name = "ckUpper";
            this.ckUpper.Size = new System.Drawing.Size(177, 17);
            this.ckUpper.TabIndex = 3;
            this.ckUpper.Text = "Символы в верхнем регистре";
            this.ckUpper.UseVisualStyleBackColor = true;
            // 
            // ckSpec
            // 
            this.ckSpec.AutoSize = true;
            this.ckSpec.Location = new System.Drawing.Point(12, 192);
            this.ckSpec.Name = "ckSpec";
            this.ckSpec.Size = new System.Drawing.Size(144, 17);
            this.ckSpec.TabIndex = 4;
            this.ckSpec.Text = "Специальные символы";
            this.ckSpec.UseVisualStyleBackColor = true;
            // 
            // ckNumber
            // 
            this.ckNumber.AutoSize = true;
            this.ckNumber.Location = new System.Drawing.Point(12, 215);
            this.ckNumber.Name = "ckNumber";
            this.ckNumber.Size = new System.Drawing.Size(62, 17);
            this.ckNumber.TabIndex = 5;
            this.ckNumber.Text = "Цифры";
            this.ckNumber.UseVisualStyleBackColor = true;
            // 
            // edLenght
            // 
            this.edLenght.Location = new System.Drawing.Point(94, 243);
            this.edLenght.Name = "edLenght";
            this.edLenght.Size = new System.Drawing.Size(120, 20);
            this.edLenght.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 245);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Длина пароля";
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(397, 281);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.edLenght);
            this.Controls.Add(this.ckNumber);
            this.Controls.Add(this.ckSpec);
            this.Controls.Add(this.ckUpper);
            this.Controls.Add(this.ckLower);
            this.Controls.Add(this.buPassword);
            this.Controls.Add(this.textBox1);
            this.Name = "fm";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.edLenght)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button buPassword;
        private System.Windows.Forms.CheckBox ckLower;
        private System.Windows.Forms.CheckBox ckUpper;
        private System.Windows.Forms.CheckBox ckSpec;
        private System.Windows.Forms.CheckBox ckNumber;
        private System.Windows.Forms.NumericUpDown edLenght;
        private System.Windows.Forms.Label label1;
    }
}

