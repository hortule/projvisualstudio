﻿using labGenPassword.Core;
using System;
using System.Windows.Forms;

namespace labGenPassword
{
    public partial class fm : Form
    {
        public fm()
        {
            InitializeComponent();
        }

        private void buPassword_Click(object sender, EventArgs e)
        {
            textBox1.Text = Utils.RandomStr((int)edLenght.Value, ckLower.Checked, ckUpper.Checked, ckNumber.Checked, ckSpec.Checked);
        }
    }
}
