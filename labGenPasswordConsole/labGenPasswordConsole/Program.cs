﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labGenPasswordConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] inp = new string[5];
            Console.WriteLine("Генерация пароля");
            Console.WriteLine("Символы в нижнем регистре [y/n]");
            inp[0] = Console.ReadLine();
            Console.WriteLine("Символы в верхнем регистре [y/n]");
            inp[1] = Console.ReadLine();
            Console.WriteLine("Специальные символы [y/n]");
            inp[2] = Console.ReadLine();
            Console.WriteLine("цифры [y/n]");
            inp[3] = Console.ReadLine();
            Console.WriteLine("Длинна пароля");
            inp[4] = Console.ReadLine();
            string c1 = "abcdefghijklmnopqrstuvwxyz";
            string c2 = "0123456789";
            string c3 = "{}[]<>,.;:-+#";
            //
            var x = new StringBuilder();
            var xResult = new StringBuilder();
            Random rnd = new Random();
            //
            if (inp[0].ToLower() == "y") x.Append(c1);
            if (inp[1].ToLower() == "y") x.Append(c1.ToUpper());
            if (inp[2].ToLower() == "y") x.Append(c2);
            if (inp[3].ToLower() == "y") x.Append(c3);
            //
            if (x.ToString() == "") x.Append(c1);
            //
            while (xResult.Length < Convert.ToInt64(inp[4]))
                xResult.Append(x[rnd.Next(x.Length)]);
            Console.WriteLine(xResult);
            
        }
    }
}
